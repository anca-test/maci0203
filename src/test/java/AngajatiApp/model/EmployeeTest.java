package AngajatiApp.model;

import AngajatiApp.repository.EmployeeImpl;
import AngajatiApp.repository.EmployeeMock;
import AngajatiApp.repository.EmployeeRepositoryInterface;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {

    EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();

    Employee e1, e2, e3, e4;

    EmployeeTest() throws Exception {
    }

    @BeforeEach
    public void SetUp() {
        e1 = new Employee();
        e2 = new Employee();
        e3 = new Employee();
        e4 = null;
        e2.setFirstName("Avram");
        e3.setFirstName("Pop");
        e2.setLastName("Anca");
        e3.setLastName("Dan");
        e2.setCnp("2880101050011");
        e3.setCnp("1850202050022");

        System.out.println("Before Test");
    }


    @Test
    @Order(1)
    void getFirstName() {
        assertEquals("", e1.getFirstName());
        assertEquals("Avram", e2.getFirstName());
        assertEquals("Pop", e3.getFirstName());
        System.out.println(" First name for e1 / e2 / e3 -  / Avram / Pop");
    }

    @Disabled
    @Test
    @Order(2)
    void getLastName_Disabled() {
        assertEquals(" ", e1.getLastName());
        System.out.println("e1 has empty field for Last Name");
    }

    @Test
    @Order(3)
    void getLastName_Compus() {
        assertEquals("Dan", e3.getLastName());
        assertEquals("Anca", e2.getLastName());
        try {
            assertEquals("", e1.getLastName());
            assert (true);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (false);
        }
        System.out.println(" Last name compus for e1 / e2 / e3 -  / Anca / Dan");
    }


    @Test
    void getCnp() {
        assertAll("Employee", (Executable)
                        (Executable) () -> assertEquals("", e1.getCnp()),
                (Executable) () -> assertEquals("2880101050011", e2.getCnp()),
                (Executable) () -> assertEquals("1850202050022", e3.getCnp()));
        System.out.println("Test all employee CNP");
    }


    @Test
    void setSalary() {
        assertEquals(0.0, e3.getSalary());
        e3.setSalary(8000.0);
        assertNotEquals("", e3.getSalary());
        assertEquals(8000.0, e3.getSalary());
        System.out.println("Set Salary of 8000.0 for employee 3");

    }

    @Test
    void constructorEmployee() {
        Employee aux = new Employee();
        assertEquals(e3.getSalary(), aux.getSalary());
        assertNotEquals(null, aux);
        System.out.println("Constructor Employee - not null salary for employee 3");
    }


    @AfterEach
    public void TearDown() {
        e1 = null;
        e2 = null;
        e3 = null;
        System.out.println("After Test");
    }

    @Test
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    public void searchEmployeeToString() {
        try {
            //Thread.sleep(80);
            TimeUnit.MILLISECONDS.sleep(80);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(e2.equals("Ion"), false);
        System.out.println("search with timeout ok");
    }


    @Test
    @Order(4)
    public void searchEmployeeToString_assertTimeout() {
        Assertions.assertTimeout(Duration.ofMillis(100), () -> {
            delay_and_call();
        }, String.valueOf(false));
        System.out.println("search with assertTimeout ok");
    }

    private void delay_and_call() {
        try {
            TimeUnit.MILLISECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ;
        e2.equals("Avram");
    }


    @ParameterizedTest
    @ValueSource(strings = {"Anca", "Dan", "Paul"})
    void testParametrizedSettingGetFirstName(String firstName) {
        e2.setFirstName(firstName);
        assertEquals(firstName, e2.getFirstName());
        System.out.println("Parametrized Set&Get with "+firstName+" - tested fine");
    }




    @Test
    public void constructorEmployee2() {
        assertThrows(NullPointerException.class,
                ()->{e4.getFirstName();});
    }

    @BeforeAll
    public static void setup () {
        System.out.println("Before Class");
    }

    @AfterAll
    public static void teardown () {
        System.out.println("After Class");
    }

}
