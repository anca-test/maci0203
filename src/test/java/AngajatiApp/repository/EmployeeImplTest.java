package AngajatiApp.repository;

import AngajatiApp.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static AngajatiApp.model.DidacticFunction.TEACHER;
import static org.junit.jupiter.api.Assertions.*;

public class EmployeeImplTest {

    EmployeeImpl employeeImpl;

    @BeforeEach
    void setup() throws Exception {
         employeeImpl = new EmployeeImpl();


    }


    //date valide
    @Test
    void addEmployeeTC1_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            //BigDecimal bd = new BigDecimal();
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("True");
            }catch (Exception e){
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC2_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("MariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaa");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC3_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC4_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName(null);
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC5_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("PopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopesc");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC6_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC7_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName(null);
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC8_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(0.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC9_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date nevalide
    @Test
    void addEmployeeTC10_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(20001.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("False");
            }catch (Exception e){
                e.printStackTrace();
                assert(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date valide
    @Test
    void addEmployeeTC11_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("PopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopes");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(19999.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("True");
            }catch (Exception e){
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date valide
    @Test
    void addEmployeeTC12_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("M");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(9.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("True");
            }catch (Exception e){
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date valide
    @Test
    void addEmployeeTC13_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Ma");
            employee.setLastName("Po");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("True");
            }catch (Exception e){
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date valide
    @Test
    void addEmployeeTC14_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("MariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMari");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(30.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("True");
            }catch (Exception e){
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date valide
    @Test
    void addEmployeeTC15_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("MariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMariaaaaaaMaria");
            employee.setLastName("Popescu");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(12000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("True");
            }catch (Exception e){
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date valide
    @Test
    void addEmployeeTC16_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("P");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(20000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("True");
            }catch (Exception e){
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //date valide
    @Test
    void addEmployeeTC17_BBT() {
        try{
            int nr_employee = employeeImpl.getEmployeeList().size(); //how many employees we had initial
            Employee employee = new Employee();
            employee.setFirstName("Maria");
            employee.setLastName("PopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPopescuuuuPope");
            employee.setCnp("2900511060025");
            employee.setFunction(TEACHER);
            employee.setSalary(3000.00);
            try{
                assertTrue( employeeImpl.addEmployee(employee));
                assertEquals(nr_employee+1,employeeImpl.getEmployeeList().size());
                System.out.println("True");
            }catch (Exception e){
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}