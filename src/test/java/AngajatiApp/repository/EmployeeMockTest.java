package AngajatiApp.repository;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeMock;
import AngajatiApp.repository.EmployeeRepositoryInterface;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static AngajatiApp.model.DidacticFunction.*;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    EmployeeRepositoryInterface employees = new EmployeeMock();


    private static Employee e1, e2;

    @BeforeEach
    public void setUp() throws Exception {
        e1 = new Employee("MARIUS", "POP", "1234567890123", ASISTENT, 2000.1);
        e2 = new Employee("DARIUS", "AVRAM", "1234567890123", ASISTENT, 2000.1);
        employees.addEmployee(e1);
        employees.addEmployee(e2);
    }

    @AfterEach
    public void tearDown() throws Exception {
        e1 = e2 = null;
    }

    @Test
    void modifyEmployeeFunctio_TC4() {
        List<Employee> employeeList = employees.getEmployeeList();
        for (Employee e : employeeList) {
            if (e.getId() == 2 )
                employees.modifyEmployeeFunction(e,ASISTENT);
            assertEquals(ASISTENT,e.getFunction());
        }
    }
    @Test
    public void modifyEmployeeFunction_T1() {
        Employee e = new Employee(null,null,null,null,null);
        employees.modifyEmployeeFunction(e, ASISTENT);
        assertNotSame(ASISTENT,e.getFunction());
    }

    @Test
    void modifyEmployeeFunctio_TC2() {
        Employee e = new Employee("Anca","POP","1234567890123",LECTURER,2000.22);
        EmployeeRepositoryInterface Employee = new EmployeeMock();

        //assertEquals(0,Employee.getEmployeeList().size());
        Employee.modifyEmployeeFunction(e,ASISTENT);
        assertNotEquals(ASISTENT,e.getFunction());
    }

    @Test
    void modifyEmployeeFunctio_TC3() {
        Employee em = new Employee("Anca","POP","1234567890123",LECTURER,2000.22);
        em.setId(2);
        employees.modifyEmployeeFunction(em,ASISTENT);
        assertNotEquals(ASISTENT,em.getFunction());
    }



}