package AngajatiApp.validator;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;

public class EmployeeValidator {
	


	public boolean isValid(Employee employee) throws EmployeeException {
		return isFirstNameValid(employee) 
				&& isLastNameValid(employee) 
				&& isCnpValid(employee) 
				&& isFunctionValid(employee) 
				&& isSalaryValid(employee);
	}

	private boolean isSalaryValid(Employee employee) throws EmployeeException {
		if (employee.getSalary() == 0){
			throw new EmployeeException("Salary not valid!");
		}
		else
			return employee.getSalary() > 0;
	}

	private boolean isFunctionValid(Employee employee) {
		return employee.getFunction().equals(DidacticFunction.ASISTENT)
				|| employee.getFunction().equals(DidacticFunction.LECTURER) 
				|| employee.getFunction().equals(DidacticFunction.TEACHER) 
				|| employee.getFunction().equals(DidacticFunction.CONFERENTIAR);
	}

	private boolean isCnpValid(Employee employee) {
		return employee.getCnp().matches("[0-9]+") && (employee.getCnp().length() == 13);
	}

	private boolean isLastNameValid(Employee employee) throws EmployeeException {
		if (!employee.getLastName().matches("[a-zA-Z]+")){
			throw new EmployeeException("Empty field");
		}
		else
			return employee.getLastName().matches("[a-zA-Z]+") && (employee.getLastName().length() > 0);
	}

	private boolean isFirstNameValid(Employee employee) throws EmployeeException {
		if (!employee.getFirstName().matches("[a-zA-Z]+")){
			throw new EmployeeException("Empty field");
		}
		else return	employee.getFirstName().matches("[a-zA-Z]+") && (employee.getFirstName().length() > 0);
	}
	
}
